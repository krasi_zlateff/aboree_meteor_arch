import { Meteor } from "meteor/meteor";
import * as Moment from 'moment';
import { Users } from "../imports/collections";

Meteor.startup(() => {

  if (Users.find({}).cursor.count() === 0) {
    Users.collection.insert({
      username: 'john',
      password: '123',
      registration: Moment().toDate()
    })
  }
});
