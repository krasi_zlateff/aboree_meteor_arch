import { Meteor } from "meteor/meteor";
import { Users } from "../imports/collections";

Meteor.publish('users', function (username: string, password: string) {
  return Users.find({
    username: username,
    password: password
  });
});
