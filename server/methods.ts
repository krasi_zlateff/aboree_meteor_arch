import { Meteor } from 'meteor/meteor';
import { Users } from "../imports/collections";

Meteor.methods({
  findUser(username: string): void {
    Users.find({
      username: username
    });
  },
});
