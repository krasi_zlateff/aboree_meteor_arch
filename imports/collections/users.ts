import { MongoObservable } from "meteor-rxjs";
import { Authenticate } from "../models";

export const Users = new MongoObservable.Collection<Authenticate>('users');
