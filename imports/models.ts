export interface Authenticate {
  username: string;
  password: string;
  registration?: Date;
}
