import { Component } from "@angular/core";
import { NavController } from "ionic-angular";

@Component({
  selector: 'welcome',
  templateUrl: 'welcome.component.html'
})

export class WelcomeComponent {
  constructor(private navCtrl: NavController) {
  }

  goToLogin() {
    this.navCtrl.push('login');
  }
}

export const welcomeComponentDeepLinkConfig = {
  component: WelcomeComponent,
  name: 'welcome',
  segment: 'welcome'
};
