import { Injectable } from '@angular/core';
import "rxjs/add/operator/map";
import { Observable } from "rxjs/Observable";
import { Authenticate } from "../../../../../../imports/models";
import { Users } from "../../../../../../imports/collections";

@Injectable()
export class AuthService {
  getUsers(): Observable<Authenticate[]> {
    return Users.find().map(users => {
      return users;
    });
  }
}
