import { Component, OnInit } from '@angular/core';
import { IonicPage } from "ionic-angular";
import { MeteorObservable } from "meteor-rxjs";
import { AuthService } from "@pages/auth/src/services/auth.service";

@IonicPage({})
@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.scss' ]
})
export class LoginComponent implements OnInit {
  users;
  authenticate = {
    username: 'john',
    password: '123'
  };

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    // MeteorObservable.subscribe('users', this.authenticate.username, this.authenticate.password).subscribe(() => {
    //   MeteorObservable.autorun().subscribe(() => {
    //     this.users = this.authService.getUsers()
    //   });
    // });
  }

  login(authenticate) {
    MeteorObservable.subscribe('users', authenticate.username, authenticate.password).subscribe(() => {
      MeteorObservable.autorun().subscribe(() => {
        this.users = this.authService.getUsers()
      });
    });
  }
}
