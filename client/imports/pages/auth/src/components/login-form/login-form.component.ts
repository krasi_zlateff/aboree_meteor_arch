import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Authenticate } from "../../../../../../../imports/models";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NavController } from "ionic-angular";

@Component({
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: [ './login-form.component.scss' ]
})
export class LoginFormComponent implements OnInit {
  @Output() submit = new EventEmitter<Authenticate>();

  constructor(private navCtrl: NavController) {

  }

  loginForm = new FormGroup({
    username: new FormControl('', [ Validators.required ]),
    password: new FormControl('', [ Validators.required ])
  });

  login() {
    this.submit.emit({
      username: this.loginForm.value.username,
      password: this.loginForm.value.password
    });
  }

  ngOnInit() {
  }

  goBack() {
    this.navCtrl.push ('welcome')
  }

}
