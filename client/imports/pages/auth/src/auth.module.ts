import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicPageModule } from "ionic-angular";
import { ReactiveFormsModule } from "@angular/forms";
import { LoginComponent } from "@pages/auth/src/containers/login/login.component";
import { LoginFormComponent } from "@pages/auth/src/components/login-form/login-form.component";
import { AuthService } from "@pages/auth/src/services/auth.service";
import { WelcomeComponent } from "@pages/welcome/welcome.component";

const COMPONENTS = [ LoginComponent, LoginFormComponent ];

@NgModule({
  imports: [
    CommonModule,
    IonicPageModule.forChild(LoginComponent),
    ReactiveFormsModule
  ],
  declarations: [ COMPONENTS ],
  exports: [ COMPONENTS ],
  providers: [ AuthService ]
})
export class AuthModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: [ AuthService ]
    }
  }
}

export const authModuleDeepLinkConfig = {
  component: LoginComponent,
  name: "login",
  segment: "login",
  defaultHistory: [ WelcomeComponent ]
};
