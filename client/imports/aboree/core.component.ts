import { Component } from '@angular/core';
import { Platform, MenuController } from 'ionic-angular';
import { Splashscreen, StatusBar } from 'ionic-native';

import { WelcomeComponent } from "@pages/welcome/welcome.component";

@Component({
  selector: 'aboree',
  templateUrl: 'core.html'
})
export class aboree {
  rootPage = WelcomeComponent;

  constructor(platform: Platform, menu: MenuController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      if (platform.is('cordova')) {
        console.log('cordova is loaded');
        StatusBar.styleDefault();
        Splashscreen.hide();
      }

      menu.enable(true);
    });
  }
}
