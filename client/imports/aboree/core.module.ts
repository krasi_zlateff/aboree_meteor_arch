import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MomentModule } from "angular2-moment";
import { DeepLinkConfig, IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { aboree } from './core.component';
import { AuthModule, authModuleDeepLinkConfig } from "@pages/auth";
import { WelcomeComponent, welcomeComponentDeepLinkConfig } from "@pages/welcome/welcome.component";

const deepLinksConfig: DeepLinkConfig = {
  links: [ authModuleDeepLinkConfig, welcomeComponentDeepLinkConfig ]
};

@NgModule({
  imports: [
    BrowserModule,
    IonicModule.forRoot(aboree, { locationStrategy: 'path' }, deepLinksConfig),
    MomentModule,
    AuthModule.forRoot()
  ],
  declarations: [
    aboree,
    WelcomeComponent
  ],
  bootstrap: [
    IonicApp
  ],
  entryComponents: [
    aboree,
    WelcomeComponent
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AboreeModule {
}
